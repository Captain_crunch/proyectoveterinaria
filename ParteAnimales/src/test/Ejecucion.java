package test;

import java.util.List;
import modelo.dao.AnimalController;
import modelo.dao.AnimalControllerImpl;
import modelo.entidades.Animales;
import modelo.excepciones.AnimalException;

public class Ejecucion {

    public static void main(String[] args) {

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			System.out.println("Error carga Drivers");
			System.exit(0);
		}

		try {
			AnimalController controller = new AnimalControllerImpl();

			List<Animales> animales = controller.list();

			for (Animales animal : animales) {
				System.out.println(animal);
			}

		} catch (AnimalException e) {
			System.out.println(e.getMessage());
		}

	}

}
