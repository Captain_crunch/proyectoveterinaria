package modelo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.entidades.Animales;
import modelo.excepciones.AnimalException;

public class AnimalControllerImpl implements AnimalController{
    
private static List<Animales> list = new ArrayList<>();
    @Override
    public List<Animales> list() throws AnimalException {
       	if (list.isEmpty()) {
            Connection connection = null;
		try {
                connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/veterinaria", 
		"root", "Pa$$w0rd");

		Statement st = connection.createStatement();

		ResultSet rs = st
		.executeQuery("SELECT a.animal_id, a.nombre, a.FechaDeNacimiento, "
                        + "a.raza, a.sexo, a.Cliente_id FROM animales a, clientes c where "
                        + "a.Cliente_id = c.cliente_id ORDER BY c.cliente_id");

		if (rs.next()) {
                    do {
						
                    Integer Animal_id = rs.getInt("animal_id");
                    String nombre = rs.getString("nombre");
                    String FechaDeNacimiento = rs.getString("FechaDeNacimiento");
                    String raza = rs.getString("raza");
                    String sexo = rs.getString("sexo");
                    Integer cliente_id = rs.getInt("Cliente_id");

                    Animales objeto = new Animales(Animal_id, nombre, FechaDeNacimiento, raza, sexo, cliente_id);
                    
                        list.add(objeto);
                        
			} while (rs.next());

			} else {
				throw new AnimalException("No hay datos en la BBDD");
				}

			} catch (SQLException ex) {
				throw new AnimalException(ex.getMessage(), ex);
			} finally {
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException ex) {
						throw new AnimalException(ex.getMessage(), ex);
					}
				}
			}
		}
		return list;
	}

}