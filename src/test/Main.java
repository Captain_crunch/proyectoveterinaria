package test;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import modelo.dao.FacturaController;
import modelo.dao.FacturaControllerImpl;
import modelo.entidades.Factura;
import modelo.excepciones.FacturaException;
import utilidades.VentanaPrincipal;

/*
contenedor principal de una clase swing=>JFrame
 */
public class Main {

    public static void main(String[] args) {
        //crear ibjeto de la clase y llamar al metodo starup()
        // Ejecucion e = new Ejecucion();

        // e.starup();
        //objeto anonimo=> objeto sin nombre que se utiliza una unica vez
        //en la instruccion de creacion
        // new Ejecucion().starup();
        //Ejeccutar el codigo del metodo staruo() con un jhilo adicional
        //para que el codigo java se pueda seguir ejecutando
        SwingUtilities.invokeLater(() -> new Main().starup());

    }

    public class miPanel extends JPanel {

        // Atributo que guardara la imagen de Background que le pasemos.
        private Image background;

        // Metodo que es llamado automaticamente por la maquina virtual Java cada vez que repinta
        public void paintComponent(Graphics g) {

            Dimension tamanio = getSize();
            ImageIcon imagenFondo = new ImageIcon(getClass().getResource("/imagenes/fondo.jpg"));
            g.drawImage(imagenFondo.getImage(), 0, 0, tamanio.width, tamanio.height, null);
            setOpaque(false);
            super.paintComponent(g);
        }

        // Metodo donde le pasaremos la dirección de la imagen a cargar.
    }

    public void starup() {

        JFrame facturasFrame = VentanaPrincipal.crear("Gestion Facturas", 600, 400, false, true);
        JFrame mainFrame = VentanaPrincipal.crear("Gestion Veterinaria", 800, 600, true, true);
        mainFrame.setLayout(new BorderLayout(50, 50));
        JButton btn1 = new JButton("Animales");
        JButton btn2 = new JButton("Clientes");
        JButton btn3 = new JButton("Facturas");
        btn3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                facturasFrame.setVisible(true);
                JPanel panel = new JPanel(new BorderLayout(10, 10));
                facturasFrame.setContentPane(panel);
                
                JList listaFacturas = new JList();
                listaFacturas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                DefaultListModel modelo = new DefaultListModel();
                modelo.addElement("Elemento1");
                modelo.addElement("Elemento2");
                modelo.addElement("Elemento3");
              
                
                try {
                    FacturaController controller= new FacturaControllerImpl();
                    
                    List<Factura> facturas = controller.lista();
                    System.out.println(facturas.size());
                    for (Factura f : facturas) {
                        System.out.println(f);
                        modelo.addElement(f);
                    }
                    
               
                } catch (Exception ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                  listaFacturas.setModel(modelo);
                panel.add(listaFacturas);
                
                facturasFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            }
        });
        miPanel pNorte = new miPanel();

        JPanel pSur = new JPanel(new GridLayout(1, 0, 10, 50));
        pSur.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        pSur.setOpaque(false);
        pSur.add(btn1);
        pSur.add(btn2);
        pSur.add(btn3);
        mainFrame.setContentPane(pNorte);
        mainFrame.add(pSur, BorderLayout.SOUTH);

        pNorte.repaint();
        mainFrame.setVisible(true);

    }
}
