
package utilidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public interface BaseDatos {
  
    String DRIVERS = "com.mysql.jdbc.Driver";
	String URL = "jdbc:mysql://localhost:3306/veterinaria";
	String USUARIO = "root";
	String CLAVE = "Pa$$w0rd";

	String SELECT_CLIENTES = "SELECT idcliente, nombre, saldo, pedidos FROM veterinaria.clientes";

	String UPDATE_CLIENTES = "UPDATE veterinaria.clientes SET saldo = ?, pedidos = pedidos + 1 WHERE idcliente = ?";

        String DELETE_CLIENTES = "DELETE FROM veterinaria.clientes + WHERE cliente_id IN (?, ?, ?)";
        
        String SELECT_FACTURAS = "SELECT Factura_id, Cliente_id, Animal_id, total FROM veterinaria.facturas";
        
        
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(URL, USUARIO, CLAVE);
	}

}
