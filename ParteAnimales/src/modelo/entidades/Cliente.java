package modelo.entidades;

public class Cliente {
      
    Integer cliente_id;
    String firstName;
    String lastName;
    Integer Telefono;
    String Email;
    String Password;
    String Dni;

    public Cliente(Integer cliente_id, String firstName, String lastName, Integer Telefono, String Email, String Password, String Dni) {
        this.cliente_id = cliente_id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.Telefono = Telefono;
        this.Email = Email;
        this.Password = Password;
        this.Dni = Dni;
    }

    public Integer getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = cliente_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getTelefono() {
        return Telefono;
    }

    public void setTelefono(Integer Telefono) {
        this.Telefono = Telefono;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getDni() {
        return Dni;
    }

    public void setDni(String Dni) {
        this.Dni = Dni;
    }



    @Override
    public String toString() {
        return  cliente_id + " " + firstName + " " + lastName + " " + Telefono +
                " " + Email + " " + Password + " " + Dni + " ";
    }
    
    
}
