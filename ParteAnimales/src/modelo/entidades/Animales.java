package modelo.entidades;

public class Animales {
    
    Integer Animal_id;
    String nombre;
    String FechaDeNacimiento;
    String raza;
    String sexo;
    Integer cliente_id;

    public Animales(Integer Animal_id, String nombre, String FechaDeNacimiento, String raza, String sexo, Integer cliente_id) {
        this.Animal_id = Animal_id;
        this.nombre = nombre;
        this.FechaDeNacimiento = FechaDeNacimiento;
        this.raza = raza;
        this.sexo = sexo;
        this.cliente_id = cliente_id;
    }

    public Integer getAnimal_id() {
        return Animal_id;
    }

    public void setAnimal_id(Integer Animal_id) {
        this.Animal_id = Animal_id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaDeNacimiento() {
        return FechaDeNacimiento;
    }

    public void setFechaDeNacimiento(String FechaDeNacimiento) {
        this.FechaDeNacimiento = FechaDeNacimiento;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Integer getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = cliente_id;
    }

    @Override
    public String toString() {
        return "Animales{" + "Animal_id=" + Animal_id + ", nombre=" + nombre + 
                ", FechaDeNacimiento=" + FechaDeNacimiento + ", raza=" + raza +
                ", sexo=" + sexo + ", cliente_id=" + cliente_id + '}';
    }
            
}
