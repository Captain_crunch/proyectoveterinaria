package modelo.excepciones;

public class AnimalException extends Exception {


    public AnimalException(String mensaje, Throwable cause) {
        super (mensaje, cause);
    }

    public AnimalException(String mensaje) {
        super(mensaje);
    }
}
