package modelo.dao;

import java.util.List;
import modelo.entidades.Factura;

public interface FacturaController {
    
    List<Factura> lista() throws Exception;

}
