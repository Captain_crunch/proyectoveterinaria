package modelo.excepciones;

public class FacturaException extends Exception {


    public FacturaException(String mensaje, Throwable cause) {
        super (mensaje, cause);
    }

    public FacturaException(String mensaje) {
        super(mensaje);
    }
}
