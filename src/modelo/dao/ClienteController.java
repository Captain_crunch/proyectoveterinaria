package modelo.dao;

import java.sql.Connection;
import java.util.Collection;
import modelo.entidades.Cliente;
import modelo.excepciones.ClienteException;

public interface ClienteController {
    
    Collection<Cliente> list() throws Exception;
    
    Cliente get(String nombreCliente) throws ClienteException;
    
    void update (Cliente cliente, Connection connection) throws ClienteException;
}
