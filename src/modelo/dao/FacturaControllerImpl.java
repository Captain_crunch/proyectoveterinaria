package modelo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.entidades.Factura;
import modelo.excepciones.FacturaException;
import utilidades.BaseDatos;

public class FacturaControllerImpl implements FacturaController {

    private static List<Factura> lista = new ArrayList<>();

    @Override
    public List<Factura> lista() throws FacturaException {
        if (lista.isEmpty()) {
            Connection connection = null;
            try {
                connection = BaseDatos.getConnection();

                PreparedStatement ps = connection.prepareStatement(
                        BaseDatos.SELECT_FACTURAS);
                
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    do {
                        int factura_id = rs.getInt("factura_id");
                        int cliente_id = rs.getInt("cliente_id");
                        int animal_id = rs.getInt("animal_id");
                        double total = rs.getDouble("total");
                        
                        Factura factura= new Factura(factura_id, cliente_id, animal_id, total);

                        lista.add(factura);

                    } while (rs.next());

                } else {
                    throw new FacturaException("No hay datos en la BBDD");
                }

            } catch (SQLException ex) {
                throw new FacturaException(ex.getMessage(), ex);
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        throw new FacturaException(ex.getMessage(), ex);
                    }
                }
            }
        }
        return lista;

    }

   
}
