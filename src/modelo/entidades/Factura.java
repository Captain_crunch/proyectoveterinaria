package modelo.entidades;

public class Factura {

    private final int factura_id;
    private Integer cliente_id;
    private Integer animal_id;
    private double total;    

    public Factura(int factura_id, Integer cliente_id, Integer animal_id, double total) {
        this.factura_id = factura_id;
        this.cliente_id = cliente_id;
        this.animal_id = animal_id;
        this.total = total;
    }

    
    
    public Integer getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = cliente_id;
    }

    public Integer getAnimal_id() {
        return animal_id;
    }

    public void setAnimal_id(Integer animal_id) {
        this.animal_id = animal_id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Factura{" + "factura_id=" + factura_id + ", cliente_id=" + cliente_id + ", animal_id=" + animal_id + ", total=" + total + '}';
    }
    
    
}
