package modelo.dao;

import java.util.List;
import modelo.entidades.Animales;
import modelo.excepciones.AnimalException;

public interface AnimalController {
    
     List<Animales> list() throws AnimalException; 
}
