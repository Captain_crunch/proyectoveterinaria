package test;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.List;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import modelo.dao.ClienteControllerImpl;
import modelo.entidades.Cliente;
import utilidades.Validacion;
import utilidades.ValidacionException;
import utilidades.VentanaPrincipal;

public class Ejecucion {

    private final JFrame mainFrame = VentanaPrincipal.crear("GESTION CLIENTES",
            300, 300, false, true);
    private final JFrame agregarFrame = VentanaPrincipal.crear("NUEVO CLIENTE",
            300, 300, false, true);
    private final JFrame eliminarFrame = VentanaPrincipal.crear("ELIMINAR CLIENTE",
            300, 300, false, true);
    private final JFrame buscarFrame = VentanaPrincipal.crear("BUSCAR CLIENTE",
            300, 300, false, true);
    private final JFrame listadoFrame = VentanaPrincipal.crear("LISTADO CLIENTE",
            300, 300, false, true);

            

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new Ejecucion().startup());
    }

    private void startup() {

        JPanel mainPanel = new JPanel(new GridLayout(2, 0, 20, 20));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));

        JButton btnCrear = new JButton("CREAR");
        JButton btnEliminar = new JButton("ELIMINAR");
        JButton btnBuscar = new JButton("BUSCAR");
        JButton btnListar = new JButton("LISTADO");


        mainPanel.add(btnCrear);
        mainPanel.add(btnEliminar);
        mainPanel.add(btnBuscar);
        mainPanel.add(btnListar);

        btnCrear.addActionListener(e -> {
            crearEmpleado();

            agregarFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

            agregarFrame.setVisible(true);
        });

        btnEliminar.addActionListener(e -> {
            eliminarEmpleado();

            eliminarFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

            eliminarFrame.setVisible(true);
        });

        btnBuscar.addActionListener(e -> {
            buscarEmpleado();


            buscarFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);


            buscarFrame.setVisible(true);
        });

        btnListar.addActionListener(e -> {
            listarEmpleados();


            listadoFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);


            listadoFrame.setVisible(true);
        });


        mainFrame.setContentPane(mainPanel);

        mainFrame.setVisible(true);
    }

    private void crearEmpleado() {
        JTextField txtCliente_id = new JTextField();
        JTextField txtFirstName = new JTextField();
        JTextField txtLastName = new JTextField();
        JTextField txtTelefono = new JTextField();
        JTextField txtEmail = new JTextField();
        JTextField txtPassword = new JTextField();
        JTextField txtDni = new JTextField();
        
        

        JLabel lblCliente_id = new JLabel("CLIENTE_ID", JLabel.RIGHT);
        JLabel lblFirstName = new JLabel("NOMBRE", JLabel.RIGHT);
        JLabel lblLastName = new JLabel("APELLIDO", JLabel.RIGHT);
        JLabel lblTelefono = new JLabel("TELEFONO", JLabel.RIGHT);
        JLabel lblEmail = new JLabel("CORREO", JLabel.RIGHT);
        JLabel lblPassword = new JLabel("CLAVE", JLabel.RIGHT);
        JLabel lblDni = new JLabel("DNI", JLabel.RIGHT);

        JPanel datos = new JPanel();
        datos.setLayout(new GridLayout(4, 0, 10, 25));
        datos.add(lblCliente_id);
        datos.add(txtCliente_id);
        datos.add(lblFirstName);
        datos.add(txtFirstName);
        datos.add(lblLastName);
        datos.add(txtLastName);
        datos.add(lblTelefono);
        datos.add(txtTelefono);
        datos.add(lblEmail);
        datos.add(txtEmail);
        datos.add(lblPassword);
        datos.add(txtPassword);
        datos.add(lblDni);
        datos.add(txtDni);

        JButton buttonOK = new JButton("CREAR");
        JButton buttonKO = new JButton("CANCELAR");

        JPanel inferior = new JPanel();
        inferior.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        inferior.add(buttonOK);
        buttonOK.addActionListener(ae -> {
            try {
                Validacion.validarEntero(txtCliente_id, true, "Cliente_id");
                Validacion.validarCadena(txtFirstName, true, "Nombre");
                Validacion.validarCadena(txtLastName, true, "Apellidos");
                Validacion.validarCadena(txtTelefono, true, "Telefono");
                Validacion.validarCadena(txtEmail, true, "Correo");
                Validacion.validarCadena(txtPassword, true, "Clave");
                Validacion.validarCadena(txtDni, true, "Dni");

            } catch (ValidacionException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error",
                        JOptionPane.ERROR_MESSAGE);

                return;
            }

            int cliente_id = Integer.parseInt(txtCliente_id.getText());
            String fistName = txtCliente_id.getText();
            String lastName = txtFirstName.getText();
            int Telefono = Integer.parseInt (txtTelefono.getText());
            String Email = txtEmail.getText();
            String Password = txtPassword.getText();
            String Dni = txtDni.getText();
            

            Cliente e = new Cliente(1, fistName, lastName, Telefono, Email, Password, Dni);

            cliente.add(e);

            agregarFrame.setVisible(false);

            JOptionPane.showMessageDialog(null, "Se ha creado un nuevo empleado");
        });

        inferior.add(buttonKO);
        buttonKO.addActionListener(ae -> {
            agregarFrame.setVisible(false);
        });

        JPanel panel = new JPanel(new BorderLayout(5, 5));

        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        panel.add(inferior, BorderLayout.SOUTH);
        panel.add(datos, BorderLayout.CENTER);

        agregarFrame.setContentPane(panel);
    }

    private void eliminarEmpleado() {

        JLabel lblSeleccion = new JLabel("SELECCIONA CLIENTE");

        Vector<Cliente> clientesV = new Vector<>(cliente);

        DefaultComboBoxModel<Cliente> modelo = new DefaultComboBoxModel<>(
                clientesV);

        JComboBox<Cliente> lista = new JComboBox<>(modelo);

        JButton buttonOK = new JButton("ELIMINAR");
        JButton buttonKO = new JButton("CANCELAR");

        JPanel inferior = new JPanel();
        inferior.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        inferior.add(buttonOK);
        buttonOK.addActionListener(ae -> {

            Cliente e = (Cliente) lista.getSelectedItem();

            cliente.remove(e);

            lista.removeItem(e);

            eliminarFrame.setVisible(false);

            JOptionPane.showMessageDialog(null, "Se ha eliminado el empleado");
        });

        inferior.add(buttonKO);
        buttonKO.addActionListener(ae -> {
            eliminarFrame.setVisible(false);
        });

        JPanel vertical = new JPanel(new GridLayout(0, 1, 10, 10));
        vertical.add(lblSeleccion);
        vertical.add(lista);

        JPanel panel = new JPanel(new BorderLayout(5, 5));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        panel.add(vertical, BorderLayout.NORTH);
        panel.add(inferior, BorderLayout.SOUTH);

        eliminarFrame.setContentPane(panel);
    }

    private void buscarEmpleado() {

    }

    private void listarEmpleados() {
        JLabel lblSeleccion = new JLabel("DATOS DE EMPLEADOS");




        DefaultListModel<Cliente> modelo = new DefaultListModel<>();

        for (Cliente e : cliente) {

            modelo.addElement(e);
        }

        JList<Cliente> lista = new JList<>(modelo);

        JButton buttonOK = new JButton("CERRAR");

        JPanel inferior = new JPanel();
        inferior.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        inferior.add(buttonOK);
        buttonOK.addActionListener(ae -> {
            listadoFrame.setVisible(false);
        });
       
        JPanel panel = new JPanel(new BorderLayout(5, 5));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        panel.add(lblSeleccion, BorderLayout.NORTH);
        panel.add(new JScrollPane(lista), BorderLayout.CENTER);
        panel.add(inferior, BorderLayout.SOUTH);

        listadoFrame.setContentPane(panel);
    }

}
