
package utilidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public interface BaseDatos {
  
    String DRIVERS = "com.mysql.jdbc.Driver";
	String URL = "jdbc:mysql://localhost:3306/veterinaria";
	String USUARIO = "root";
	String CLAVE = "Pa$$w0rd";


	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(URL, USUARIO, CLAVE);
	}

}
