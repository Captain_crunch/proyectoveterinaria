package utilidades;

import javax.swing.JTextField;
import utilidades.ValidacionException;

public abstract class Validacion {

    public static void validarEntero(JTextField field, boolean required, String nombre) throws ValidacionException {
        String contenidoCaja = field.getText();

        if (required) {
            if (contenidoCaja.trim().length() == 0) {
                throw new ValidacionException("La caja " + nombre + " es obligatoria");
            }
        }

        if (!contenidoCaja.matches("[0-9]+")) {
            throw new ValidacionException("La caja " + nombre + " ha de tener un valor entero");
        }
    }

    public static void validarCadena(JTextField field, boolean required, String nombre) throws ValidacionException {
        String contenidoCaja = field.getText();

        if (required) {
            if (contenidoCaja.trim().length() == 0) {
                throw new ValidacionException("La caja " + nombre + " es obligatoria");
            }
        }

        if (!contenidoCaja.matches("([A-Za-zñáéíóú.+]+[\\s]*)+")) {
            throw new ValidacionException("La caja " + nombre + " ha de contener unicamente letras");
        }
    }
}
