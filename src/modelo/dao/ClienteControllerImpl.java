package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import modelo.entidades.Cliente;
import modelo.excepciones.ClienteException;
import utilidades.BaseDatos;


public class ClienteControllerImpl implements ClienteController {

        private static Map<String, Cliente> clientes = null;

    @Override
    public Collection<Cliente> list() throws Exception {
         if (clientes == null) {
             Connection connection = null;
             try {
              connection = BaseDatos.getConnection();
                 
                PreparedStatement ps = connection.prepareStatement(
                        BaseDatos.SELECT_CLIENTES);
                
                ResultSet rs = ps.executeQuery();
                
                if (rs.next()) {
                    clientes = new Hashtable<>();
                    do {
                        int cliente_id = rs.getInt("cliente_id");
                        String firstName = rs.getString("firstName");
                        String lastName = rs.getString ("lastName");
                        Integer Telefono = rs.getInt ("Telefono");
                        String Email = rs.getString ("Email");
                        String Password = rs.getString ("Password");
                        String Dni = rs.getString ("Dni");
                        
                        Cliente cliente = new Cliente(cliente_id, firstName,
                                lastName, Telefono, Email, Password, Dni);
                    } while (rs.next());
                } else {
                    throw new ClienteException("No hay Clientes en la BBDD");
                }
             } catch (SQLException e) {
                 throw new ClienteException(e.getMessage(), e);
             } finally {
                 if (connection != null) {
                     try {
                         connection.close();
                     } catch (SQLException e) {
                         throw new ClienteException(e.getMessage(), e);
                     }
                 }
             }
         }
         return clientes.values();
    }

    @Override
    public Cliente get(String firstNameCliente) throws ClienteException {
         if (clientes == null) {
             
         }
         if (clientes.containsKey(firstNameCliente)) {
             return clientes.get(firstNameCliente);
         }
         throw new ClienteException("No hay cliente con nombre " + firstNameCliente);
    }

    @Override
    public void update(Cliente cliente, Connection connection) throws ClienteException {
         try {
             PreparedStatement ps = connection.prepareStatement(
                            BaseDatos.UPDATE_CLIENTES);
             
             
         } catch (SQLException e) {
            throw new ClienteException(e.getMessage(), e);
        }
    }
    
}
