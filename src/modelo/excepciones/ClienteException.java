package modelo.excepciones;

public class ClienteException extends Exception {


    public ClienteException(String mensaje, Throwable cause) {
        super (mensaje, cause);
    }

    public ClienteException(String mensaje) {
        super(mensaje);
    }
}
